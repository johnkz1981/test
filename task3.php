<?php
/**
 * Задача №3
 * Нужно взять chart.json и написать скрипт который на выходе будет отдавать chart_result.json
 */
$content = file_get_contents('chart.json');
$arrChart = json_decode($content);

foreach ($arrChart as &$row){
    if(count($row) < 5){
        continue;
    }
    for($i = 1; $i < 5; $i++){
        if($row[$i] !== 100){
            continue;
        }
    }
    for($i = 1; $i < count($row); $i++){
        if($row[$i] !== 100){
            continue;
        }
        $row[$i] = null;
    }
}
file_put_contents('chart_result.json', json_encode($arrChart));