<?php
use \Bitrix\Iblock\Elements\ElementCatalog1cApiTable as Catalog1c;
/**
 * Задача №2
 * написать скрипт который будет добавлять товар и его торговые предложения
 * Задача должна быть реализована как отдельный скрипт с использованием Bitrix API
 */

/**
 * Основные поля
 */
$name = 'Лампа светодиодная "StarLight"';
$arParams = array("max_len" => 220);
$translit = Cutil::translit($name, "ru", $arParams);
$code = $translit;

$sections = [1, 6, 8];

$elem = Catalog1c::createObject();
$elem->set('NAME', $name);
$elem->set('PREVIEW_TEXT', 'Лампа светодиодная "StarLight" T10 W2.1x9.5d 12В 9SMD белый в блистере');
$elem->set('IN_SECTIONS', 'Y');
$elem->set('IBLOCK_ID', 8);
$elem->set('ACTIVE', 'Y');
$elem->set('IBLOCK_SECTION_ID', $sections[0]);
$elem->set('CODE', $code);
$elem->save();

$id = $elem->getId();

/**
 * Разделы
 */
\CIBlockElement::SetElementSection($id, $sections);

/**
 * Свойства элемента
 */
$elem = Catalog1c::getByPrimary($id)->fetchObject();

$elem->addTo(ARTICLE2, new PropertyValue('1928'));
$elem->set(SEARCH2, new PropertyValue('1928'));
$elem->addTo(BRAND2, new PropertyValue('f054abda-f375-11e6-8683-086266a27723'));

$analogs = [5, 54, 89];

foreach ($analogs as $analog){
    $elem->addTo('ANALOGI', new PropertyValue($analog));
}
$elem->save();

/**
 * Торговый каталог
 */
$PRICE_TYPE_ID = 11;

$arFields = Array(
    "PRODUCT_ID" => $id,
    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
    "PRICE" => 87,
    "CURRENCY" => "RUB",
);
$arPrice = Price::GetList([
    'select' => array('*'),
    'filter' => array(
        "PRODUCT_ID" => $id,
        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID
    )
]);
if ($price = $arPrice->Fetch())
{
    Price::Update($price["ID"], $arFields);
}
else
{
    Price::Add($arFields);
}
$productFileds = array(
    "ID" => $id,
    "VAT_ID" => 3,
    "VAT_INCLUDED" => "Y",
    "TYPE" => \Bitrix\Catalog\ProductTable::TYPE_PRODUCT,
    'QUANTITY' => 15
);

if(!ProductTable::getRowById($id)){
    if(ProductTable::Add($productFileds)){
        echo 'Элемент инфоблока превращен в товар';
    }else{
        echo 'Произошла ошибка';
    }
}else{
    ProductTable::Update($id, $productFileds);
}

/**
 * Картинка
 */
$tmp = "{$_SERVER['DOCUMENT_ROOT']}/upload/tmp/tmp.jpg";
$file = \CFile::MakeFileArray(
    $tmp
);
$fileSave = \CFile::SaveFile(
    $file,
    '/iblock'
);
$elem = Catalog1c::getById($id)->fetchObject();
$elem->set('PREVIEW_PICTURE', $fileSave);
$elem->set('DETAIL_PICTURE', $fileSave);
$elem->save();