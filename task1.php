<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
<?
/**
 * Задача №1
 * найти и убить файлы от удаленных товаров (или от старого сайта),
 * которые лежат вместе с картинками товаров в одной директории на диске
 *
 * Пошел по самому простому пути.
 * Скопировал все необхадимые файлы во временную папку,
 * удалил рабочую папку и обратно скопировал все файлы.
 */


$dir = $_SERVER["DOCUMENT_ROOT"] . '/tmp';
if (!file_exists($dir)) {
    mkdir($dir, 0777, true);
}

$arOrder = array(
    "ID" => "ASC",
);
$arFilter = ["IBLOCK_ID"=> "10"];
$arSelect = Array("ID", "NAME", 'PREVIEW_PICTURE', 'DETAIL_PICTURE', "PROPERTY_MORE_PHOTO");

$rsElements = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

while ($rsElement = $rsElements->fetch()){
    if($rsElement['PREVIEW_PICTURE']){
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . CFile::GetPath($rsElement['PREVIEW_PICTURE']),
            $_SERVER["DOCUMENT_ROOT"] . '/tmp' . CFile::GetPath($rsElement['PREVIEW_PICTURE']));
    }
    if($rsElement['DETAIL_PICTURE']){
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . CFile::GetPath($rsElement['DETAIL_PICTURE']),
            $_SERVER["DOCUMENT_ROOT"] . '/tmp' . CFile::GetPath($rsElement['DETAIL_PICTURE']));
    }
    foreach($rsElement['PROPERTY_MORE_PHOTO_VALUE'] as $photo){
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . CFile::GetPath($photo), $_SERVER["DOCUMENT_ROOT"] . '/tmp' . CFile::GetPath($photo));
    }
}
$imgDir = $_SERVER["DOCUMENT_ROOT"] . '/upload/iblock';
$tmpImgDir = $_SERVER["DOCUMENT_ROOT"] . '/tmp/upload/iblock';
`rm -rf $imgDir`;
`cp -r $tmpImgDir $imgDir`;

?>
<? require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>